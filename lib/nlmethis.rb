require 'pp'
require 'csv'
require "yaml"
require "lemmatizer"
require 'stopwords'
require 'lingua/stemmer'

class NLmeThis


    
    def initialize()
        @wordRank = {}
        @lemmatize = Lemmatizer.new
        @stemmer   = Lingua::Stemmer.new(:langiage => "en")
    end

    def setWord(word)
        @word = word
    end

    def satitize()
        
    end

    def baseWord ()
        if (! Stopwords.is?(@word))
            lem = Lemmatizer.new
            sanitizeRE = /('|\.|\"|‘|’|\/|\\)/
            @word = @word.downcase.gsub(sanitizeRE, '')
            @word = Lingua.stemmer(@word)
            @word = lem.lemma(@word, :noun)
            if ( @wordRank.key?(@word) ) 
                @wordRank[@word] = @wordRank[@word] + 1 
            else
                @wordRank[@word] = 1
            end

            @word
        else
            return 0
        end
    end



    def ppIt()
        @wordRank.keys.sort {|a, b| h[b] <=> h[a]}
    end
end

comment = NLmeThis.new

table = CSV.parse(File.read("csat.bad.csv"), headers: true)
table.by_col[2].each do |l|
    l.split.each do |w|
        comment.setWord(w)
        comment.baseWord
    end
end

comment.ppIt

